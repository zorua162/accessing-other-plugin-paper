package me.zorua162.mainplugintest;

import org.bukkit.plugin.java.JavaPlugin;

public class PersistenceWrapper implements MainPluginTestAPI{


    private final MainPluginTest mainPluginTest;

    public PersistenceWrapper(MainPluginTest mainPluginTest) {
        this.mainPluginTest = mainPluginTest;
    }
    @Override
    public String getVar() {
        return mainPluginTest.getVar();
    }
}
