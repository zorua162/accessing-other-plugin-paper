package me.zorua162.mainplugintest;

import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.java.JavaPlugin;

public class MainPluginTest extends JavaPlugin {

    // Old attempt using static variable:
    // public static MainPluginTest INSTANCE;
    public String var;
    // public String var2;
    // public String variable = "Old";


    // New attempt using ServicesManager

    @Override
    public void onEnable() {
        // INSTANCE = this;
        var = "Stuff";
        // INSTANCE.var2 = "Things";

        // Plugin startup logic
        PersistenceWrapper mainPluginTestAPI = new PersistenceWrapper(this);


        getServer().getServicesManager().register(MainPluginTestAPI.class, mainPluginTestAPI, this, ServicePriority.High);



    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public String getVar() {
        return var;
    }
}
