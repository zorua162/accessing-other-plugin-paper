Goal: To get the instance of MainPluginTest in SubProject so that the sub plugin is able to access
variables in the main plugin such as getting the contents of "var" from MainPlugin into SubPlugin

With thanks to this post from 2011 which helped me get the initial code:
https://bukkit.org/threads/services-api-intro.26998/

And world border api for helping me find that the provided scope is required in the pom.xml of the sub project.
I.e: 
```
        <dependency>
            <groupId>me.zorua162</groupId>
            <artifactId>MainPluginTest</artifactId>
            <version>1.0</version>
            <scope>provided</scope>
        </dependency>
```
