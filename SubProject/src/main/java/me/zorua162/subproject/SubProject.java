package me.zorua162.subproject;

import me.zorua162.mainplugintest.MainPluginTest;
import me.zorua162.mainplugintest.MainPluginTestAPI;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import net.milkbowl.vault.chat.Chat;

public class SubProject extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        // String bar = MainPluginTest.INSTANCE.var;

        // RegisteredServiceProvider<MainPluginTest> mainPluginTestRegisteredServiceProvider = getServer().getServicesManager().getRegistration(MainPluginTest.class);
        // MainPluginTest mainPluginTest = mainPluginTestRegisteredServiceProvider.getProvider();

        // MainPluginTestAPI mainPluginTest = getServer().getServicesManager().load(MainPluginTestAPI.class);
        RegisteredServiceProvider<MainPluginTestAPI> mainPluginTestAPIRegisteredServiceProvider = getServer().getServicesManager().getRegistration(MainPluginTestAPI.class);
        if (mainPluginTestAPIRegisteredServiceProvider==null) {
            getLogger().info("MainPluginTest is null, disabling plugin and aborting");
            setEnabled(false);
            return;
        } else {

            getLogger().info(mainPluginTestAPIRegisteredServiceProvider.getProvider().getVar());
        }


    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
